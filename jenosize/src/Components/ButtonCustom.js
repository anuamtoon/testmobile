import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
const ButtonCustom = props => {
  return (
    <TouchableOpacity
      style={{
        width: 200,
        height: 32,
        borderRadius: 6,
        backgroundColor: `${props.colors}`,
        borderWidth: 1,
        borderColor: 'gray',
        alignItems: 'center',
        justifyContent: 'center',
      }}
      onPress={()=> props.navigation.navigate(`${props.Screen}`)}
      >
      <Text style={{fontSize:16,fontWeight:'700'}}>{props.Text}</Text>
    </TouchableOpacity>
  );
};

export default ButtonCustom;
