export const Images = {
    logo: require('../../assets/image/logo.png'),
    iconBack: require('../../assets/image/icon-back.png'),
    iconMore: require('../../assets/image/more-active-icon.png'),
    iconThumbnail: require('../../assets/image/thumbnail.png'),
    iconPin: require('../../assets/image/icon_pin.png'),
  };
  