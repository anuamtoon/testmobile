import React from 'react'
import {View,TouchableOpacity,Image,SafeAreaView} from 'react-native'
import {Images} from '../Components/Images' 
import colors from '../helpers/colors' 
const HeaderBar = (props) => {
    return (
        <SafeAreaView style={{height: 30,paddingHorizontal:14,backgroundColor:colors.backgroud,justifyContent: 'center'}}>
           <View style={{justifyContent: 'space-between',alignItems: 'center',flexDirection: 'row'}}>
            <TouchableOpacity style={{width:16}} onPress={()=> props.navigation.goBack()}>
                <Image source={Images.iconBack} style={{width:20,height:20}} resizeMode='contain' />
            </TouchableOpacity>
            <TouchableOpacity style={{width:16}}>
                <Image source={Images.iconMore} style={{width:20,height:20}} resizeMode='contain' />
            </TouchableOpacity>
        </View>
        </SafeAreaView>
        
    )
}

export default HeaderBar