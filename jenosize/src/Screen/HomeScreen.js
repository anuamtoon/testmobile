import React from 'react';
import {View, SafeAreaView,StyleSheet, Image} from 'react-native';
import {Images} from '../Components/Images';
import {ButtonCustom} from '../Components';
import colors from '../helpers/colors';
const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.Container}>
      <View style={styles.Content}>
        <View style={styles.Logo}>
          <Image
            source={Images.logo}
            style={styles.ImageStyle}
            resizeMode="cover"
          />
        </View>
        <View style={styles.Button}>
          <View style={styles.ButtonInner}>
            <ButtonCustom
              Text="ค้นหาร้านอาหาร"
              colors={'white'}
              navigation={navigation}
              Screen={'Search'}
            />
          </View>
          <View style={styles.ButtonInner}>
            <ButtonCustom
              Text="แผนที่ บริษัท Jenosize"
              colors={'white'}
              navigation={navigation}
              Screen={'Map'}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: colors.backgroud,
  },
  Content: {
    flex: 1,
    alignItems: 'center',
  },
  Logo: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 120,
  },
  ImageStyle: {
    width: 200,
    height: 150,
  },
  Button: {
    flex: 1,
    justifyContent: 'center',
  },
  ButtonInner: {
    paddingVertical: 8,
  },
});

export default HomeScreen;
