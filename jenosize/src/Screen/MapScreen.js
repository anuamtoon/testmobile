import React from 'react';
import MapView from 'react-native-maps';
import {SafeAreaView, View} from 'react-native';
import {HeaderBar} from '../Components';
import {Marker} from 'react-native-maps';
import {Images} from '../Components/Images';
const MapScreen = ({navigation}) => {
  return (
    <React.Fragment>
      <SafeAreaView>
        <HeaderBar navigation={navigation} />
      </SafeAreaView>
      <View style={{flex: 1}}>
        <MapView
          style={{flex: 1}}
          region={{
            latitude: 13.894009,
            longitude: 100.51892355,
            latitudeDelta: 0.0022,
            longitudeDelta: 0.0101,
          }}>
          <Marker
            coordinate={{latitude: 13.894009, longitude: 100.51892355}}
            // image={Images.iconPin}
            
          />
        </MapView>
      </View>
    </React.Fragment>
  );
};
export default MapScreen;
