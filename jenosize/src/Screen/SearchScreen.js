import React, {useState} from 'react';
import {
  View,
  SafeAreaView,
  Text,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';
import {HeaderBar} from '../Components';
import colors from '../helpers/colors';
import axios from 'axios';
const SearchScreen = ({navigation}) => {
  const [search, setSearch] = useState('');
  const [movies, setMovie] = useState();

  const options = {
    method: 'GET',
    // url: 'https://api.themoviedb.org/3/movie/550?api_key=c212b249c37cd55da07efbec0133c0c7',
    url: 'https://order-pizza-api.herokuapp.com/api/orders',
  };

  axios
    .request(options)
    .then(function (response) {
      setMovie(response.data);
    })
    .catch(function (error) {
      console.error(error);
    });
  const Item = props => (
    <TouchableOpacity style={styles.CarItems}>
      <Image style={styles.ImageStyle} resizeMode="contain" />
      <View>
        <Text>
          <Text style={styles.TextStyle}>Order Name :: </Text>
          {props.Flavor}
        </Text>
        <Text>
          <Text style={styles.TextStyle}>Size :: </Text>
          {props.Size}
        </Text>
        <Text>
          <Text style={styles.TextStyle}>TableNo ::</Text>
          {props.TableNo}
        </Text>
        <Text>
          <Text style={styles.TextStyle}>Crust ::</Text>
          {props.Crust}
        </Text>
      </View>
    </TouchableOpacity>
  );
  const renderItem = ({item}) => (
    <Item
      Flavor={item.Flavor}
      Size={item.Size}
      TableNo={item.Table_No}
      Crust={item.Crust}
    />
  );
  const updateSearch = search => {
    setSearch({search});
  };
  return (
    <SafeAreaView style={styles.Container}>
      <HeaderBar navigation={navigation} />
      <TouchableOpacity style={styles.Search}></TouchableOpacity>
      <View style={styles.Content}>
        <FlatList
          data={movies}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: colors.backgroud,
  },
  Content: {
    padding: 24,
    flex: 1,
  },
  Search: {
    borderWidth: 1,
    borderRadius: 6,
    borderColor: colors.greyish,
    height: 40,
    marginHorizontal: 24,
    marginTop: 16,
  },
  CarItems: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 6,
  },
  ImageStyle: {
    width: 120,
    height: 120,
    backgroundColor: 'gray',
    borderRadius: 6,
    marginRight: 24,
  },
  TextStyle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default SearchScreen;
